package com.example.service;

import com.example.model.User;

public interface UserService {

	void save(User userForm);

	User findByUsername(String a);

	double findByName(String name);

	double findByNameCustomer(String name);

	double findByNameAffiliate(String name);
}
