package com.example.service.impl;

import java.util.HashSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.dao.ProductRepo;
import com.example.dao.RoleRepository;
import com.example.dao.UserRepo;
import com.example.model.Product;
import com.example.model.User;
import com.example.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	@Autowired
	UserRepo userRepo;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	ProductRepo repo;

	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public void save(User userForm) {
		System.out.println("entered");
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		System.out.println("entered");
		userForm.setPassword(bCryptPasswordEncoder.encode(userForm.getPassword()));
		System.out.println("entered");
		System.out.println(roleRepository.findAll());
		userForm.setRoles(new HashSet<>(roleRepository.findAll()));

		System.out.println("entered");
		userRepo.save(userForm);
	}

	@Override
	public User findByUsername(String a) {
		return userRepo.findByUsername(a);

	}

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	public double findByName(String name) {
		try {
			logger.info("Into the  Employers Products Service Layer");
			Product prod = repo.findByName(name);
			double rate = prod.getRate();
			double rates = (.3 * rate);
			double discount = rate - rates;
			return discount;
		} catch (Exception e) {
			return 0;
		}

	}

	@Override
	public double findByNameCustomer(String name) {
		try {
			logger.info("Into the Customers Products Service Layer");
			Product prod = repo.findByName(name);
			double rate = prod.getRate();
			double rates = (.1 * rate);
			double discount = rate - rates;
			return discount;
		} catch (Exception e) {
			return 0;
		}

	}

	@Override
	public double findByNameAffiliate(String name) {
		try {
			logger.info("Into the Affiliate Products Service Layer");
			Product prod = repo.findByName(name);
			double rate = prod.getRate();
			double rates = (.05 * rate);
			double discount = rate - rates;
			return discount;
		} catch (Exception e) {
			return 0;
		}

	}
}
