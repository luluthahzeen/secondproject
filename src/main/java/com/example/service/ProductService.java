package com.example.service;

import com.example.model.Product;

public interface ProductService {

	Product findByName(String name);
}
