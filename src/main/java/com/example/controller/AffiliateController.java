package com.example.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.model.Product;
import com.example.service.UserService;

@Controller
@RequestMapping("/affiliate")
public class AffiliateController {
	Logger logger = LoggerFactory.getLogger(AffiliateController.class);

	@Autowired
	private UserService productServices;

	@RequestMapping(value = "profile")
	public ModelAndView view() {
		ModelAndView mv = new ModelAndView();
		try {
			logger.info("Into the Affiliate Profile Page");
			UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			String username = userDetails.getUsername();
			mv.setViewName("/employer.jsp");
			mv.addObject("loggedInUser", username);
			return mv;

		} catch (Exception e) {
			mv.setViewName("/error.jsp");
			logger.error("Error occured", e);
			return mv;
		}
	}

	@RequestMapping("buy")
	public ModelAndView buyproducts(@ModelAttribute Product product) {
		ModelAndView mv = new ModelAndView();
		try {
			logger.info("Into ControllersBuy");
			String name = product.getName();
			double rate = productServices.findByNameAffiliate(name);
			System.out.println(rate);
			mv.setViewName("/success.jsp");
			mv.addObject("rate", rate);
			return mv;
		} catch (Exception e) {
			mv.setViewName("/error.jsp");
			logger.error("Error occured", e);
			return mv;

		}
	}
}
